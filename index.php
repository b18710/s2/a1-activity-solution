<?php require_once "./code.php" ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>a2 -activity 2</title>
</head>
<body>

	<h1>Activity 1</h1>
	<?php Loop(); ?>

	<h1>Activity 2</h1>
	
	<?php $students = array(); ?>

	<?php array_push($students,'John Smith'); ?>
	<p><?php echo var_dump($students); ?></p>
	<p><?= count($students); ?></p>

	<?php array_push($students,'Jane Smith'); ?>
	<p><?php echo var_dump($students); ?></p>
	<p><?= count($students); ?></p>

	<?php array_shift($students); ?>
	<p><?php echo var_dump($students); ?></p>
	<p><?= count($students); ?></p>


</body>
</html>